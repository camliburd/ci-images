# ci-images

CI images for infrastructure projects. Each directory contains a Dockerfile for
one image. [.gitlab-ci.yml](.gitlab-ci.yml) describes the pipelines that build
Docker images that are kept in this project's registry. Images are tagged
`registry.gitlab.com/gitlab-com/gl-infra/ci-images/${image_name}:${short_sha}`,
and images built from master are tagged
`registry.gitlab.com/gitlab-com/gl-infra/ci-images/${image_name}:latest`.

## Adding a new image

Create a directory in this repository and add a Dockerfile.

Add 2 jobs to `.gitlab-ci.yml`: `build_$image_name` and `latest_$image_name`.
Use existing jobs as examples. There is not much config for each job, so this
should be relatively simple.

Make a merge request. An image should be built and pushed to this project's
registry. When this MR is merged to master, another image will be built and
tagged with the master short SHA, and also "${image_name}-latest".
